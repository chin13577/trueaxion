﻿using UnityEngine;

public interface ITakeDamageable
{
    GameObject gameObject { get; }
    void TakeDamage(DamageData damage);
    Team Team { get; }
    //bool IsDead { get; }
}