﻿
using System;

public abstract class CentipedeMovementState
{
    protected Centipede Owner { get { return _brain.Owner; } }
    protected CentipedeAI _brain;

    public abstract CentipedeAI.MoveStateType GetStateType { get; }

    protected Direction? _orderNextDirection;


    public CentipedeMovementState(CentipedeAI brain)
    {
        _brain = brain;
    }

    public abstract void OnEnter();
    public abstract void OnExit();

    /// <summary>
    /// Evaluate next direction for Centipede.
    /// </summary>
    /// <param name="direction">next direction</param>
    /// <returns>The success of evaluating</returns>
    public abstract bool EvaluateDirection(out Direction nextDirection);

    public void SetOrderNextDirection(Direction direction)
    {
        _orderNextDirection = direction;
    }

    protected bool CheckCanMoveByOrderDirection(out Direction direction)
    {
        if (_orderNextDirection.HasValue)
        {
            direction = _orderNextDirection.Value;
            ClearOrderNextDirection();
            return true;
        }
        else
        {
            direction = 0;
            ClearOrderNextDirection();
            return false;
        }
    }

    private void ClearOrderNextDirection()
    {
        _orderNextDirection = null;
    }


}
