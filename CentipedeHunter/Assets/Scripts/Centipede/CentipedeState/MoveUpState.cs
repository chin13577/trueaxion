﻿using UnityEngine;

public class MoveUpState : CentipedeMovementState
{
    public override CentipedeAI.MoveStateType GetStateType => CentipedeAI.MoveStateType.Up;

    public MoveUpState(CentipedeAI brain) : base(brain)
    {
    }

    public override void OnEnter()
    {
    }

    public override void OnExit()
    { 
    }

    public override bool EvaluateDirection(out Direction nextDirection)
    {
        if (CheckCanMoveByOrderDirection(out nextDirection))
        {
            return true;
        }


        Direction currentDirection = Owner.Head.CurrentDirection;
        Direction oldHorizonDirection = Owner.Head.OldHorizonDirection;
        if (currentDirection == Direction.Down)
        {
            nextDirection = currentDirection;
            return false;
        }
        if (currentDirection == Direction.Up)
        {
            return CheckNextMoveToHorizontalDirection(out nextDirection, currentDirection, oldHorizonDirection);
        }
        else
        { 
            bool isCanMove = Owner.IsCanMove(currentDirection);
            if (isCanMove)
            {
                nextDirection = currentDirection;
                return true;
            }
            else
            {
                if (Owner.IsCanMove(Direction.Up))
                {
                    nextDirection = Direction.Up;
                    return true;
                }
                else
                {
                    nextDirection = currentDirection;
                    return false;
                }
            }
        }
    }

    private bool CheckNextMoveToHorizontalDirection(out Direction nextDirection, Direction currentDirection, Direction oldHorizonDirection)
    {
        if (oldHorizonDirection == Direction.Left)
        {
            bool isCanMove = Owner.IsCanMove(Direction.Right);
            nextDirection = isCanMove ? Direction.Right : currentDirection;
            return isCanMove;
        }
        else
        {
            bool isCanMove = Owner.IsCanMove(Direction.Left);
            nextDirection = isCanMove ? Direction.Left : currentDirection;
            return isCanMove;
        }
    }
}