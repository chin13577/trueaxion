﻿
using UnityEngine;

public class MoveDownState : CentipedeMovementState
{
    public override CentipedeAI.MoveStateType GetStateType => CentipedeAI.MoveStateType.Down;


    public MoveDownState(CentipedeAI brain) : base(brain)
    {
    }

    public override void OnEnter()
    {
        // do something.
    }

    public override void OnExit()
    {
    }

    public override bool EvaluateDirection(out Direction nextDirection)
    {
        if (CheckCanMoveByOrderDirection(out nextDirection))
        {
            return true;
        }

        Direction currentDirection = Owner.Head.CurrentDirection;
        Direction oldHorizonDirection = Owner.Head.OldHorizonDirection;
        if (currentDirection == Direction.Up)
        {
            nextDirection = currentDirection;
            return false;
        }
        if (currentDirection == Direction.Down)
        {
            return CheckNextMoveToHorizontalDirection(out nextDirection, currentDirection, oldHorizonDirection);
        }
        else
        { 
            bool isCanMove = Owner.IsCanMove(currentDirection);
            if (isCanMove)
            {
                nextDirection = currentDirection;
                return true;
            }
            else
            {
                if (Owner.IsCanMove(Direction.Down))
                {
                    nextDirection = Direction.Down;
                    return true;
                }
                else
                {
                    nextDirection = currentDirection;
                    return false;
                }
            }
        }
    }

    private bool CheckNextMoveToHorizontalDirection(out Direction nextDirection, Direction currentDirection, Direction oldHorizonDirection)
    {
        if (oldHorizonDirection == Direction.Right)
        {
            bool isCanMove = Owner.IsCanMove(Direction.Left);
            nextDirection = isCanMove ? Direction.Left : currentDirection;
            return isCanMove;
        }
        else
        {
            bool isCanMove = Owner.IsCanMove(Direction.Right);
            nextDirection = isCanMove ? Direction.Right : currentDirection;
            return isCanMove;
        }
    }
}
