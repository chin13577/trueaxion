﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentipedeCell : MonoBehaviour, ITakeDamageable
{
    public Team Team => Team.Enemy;
    public enum CellType { Body = 0, Head }

    public Direction OldHorizonDirection { get => _oldHorizonDirection; private set => _oldHorizonDirection = value; }
    private Direction _oldHorizonDirection = Direction.Right;

    public Direction CurrentDirection { get => _currentDirection; private set => _currentDirection = value; }
    private Direction _currentDirection = Direction.Right;

    public Direction LastDirection { get => _lastDirection; private set => _lastDirection = value; }
    private Direction _lastDirection = Direction.Right;

    public CellType Type { get; set; }
    public Vector3 LastPosition;
    public Vector3 CurrentPosition
    {
        get
        {
            if (this != null)
                return transform.position;
            return new Vector3(int.MinValue, int.MinValue);
        }
    }

    public CentipedeCell Parent;
    public CentipedeCell Child;

    private Centipede _owner;

    public Status status;

    public MapManager Map
    {
        get
        {
            if (_map == null)
                _map = GameManager.Instance.MapManager;
            return _map;
        }
    }
    private MapManager _map;
    [SerializeField] private SpriteRenderer _spriteRenderer;

    public void Initialize(Centipede owner)
    {
        this._owner = owner;

        Type = CellType.Body;
        LastPosition = CurrentPosition;
        status = GameSetting.Instance.GetCentipedeCellStatus();
    }

    public bool IsHead()
    {
        if (_owner.Head == null)
        {
            return false;
        }
        return _owner.Head == this;
    }

    public void SetSprite(CellType type)
    {
        MonsterSpawner monsterSpawner = GameManager.Instance.MonsterSpawner;
        switch (type)
        {
            case CellType.Body:
                _spriteRenderer.sprite = monsterSpawner.GetTailSprite();
                _spriteRenderer.color = Color.white;
                break;
            case CellType.Head:
                _spriteRenderer.sprite = monsterSpawner.GetHeadSprite();
                _spriteRenderer.color = Color.red;
                break;
            default:
                break;
        }
    }

    public void SetParent(CentipedeCell cell)
    {
        Parent = cell;
    }

    public void SetChild(CentipedeCell cell)
    {
        Child = cell;
    }

    public void ClearChildAndParent()
    {
        if (Parent != null)
            Parent.Child = null;
        if (Child != null)
            Child.Parent = null;
        Child = null;
        Parent = null;
    }

    public void SetNewPosition(Vector3 position)
    {
        LastPosition = CurrentPosition;
        Map.RemoveObjectFromLandData(this.gameObject);

        transform.position = position;
        Map.SetObjectToLandData(this.gameObject);
    }

    public void SetNewDirection(Direction newDirection)
    {
        if (newDirection != Direction.Up && newDirection != Direction.Down)
        {
            OldHorizonDirection = newDirection;
        }
        LastDirection = CurrentDirection;
        CurrentDirection = newDirection;
    }

    public void Move()
    {
        if (Parent != null)
        {
            SetNewDirection(Parent.LastDirection);
            SetNewPosition(Parent.LastPosition);
        }

        if (Child != null)
        {
            Child.Move();
        }
    }

    public void TakeDamage(DamageData damageData)
    {
        status.HP -= damageData.Damage;
        if (status.IsDead)
        {
            _owner.OnCellDead(this);
        }
    }

}
