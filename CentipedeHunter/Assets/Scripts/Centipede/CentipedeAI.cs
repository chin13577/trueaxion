﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentipedeAI
{
    public enum MoveStateType { Up, Down };
    public Centipede Owner { get; private set; }
    private CentipedeMovementState CurrentState;

    public CentipedeAI(Centipede owner)
    {
        this.Owner = owner;
        CurrentState = GetMovementState(MoveStateType.Down);
        CurrentState.OnEnter();
    }

    /// <summary>
    /// Create with Coppy value.
    /// </summary> 
    public CentipedeAI(Centipede owner, MoveStateType stateType)
    {
        this.Owner = owner;
        CurrentState = GetMovementState(stateType);
    }

    public MoveStateType GetMoveStateType()
    {
        return CurrentState.GetStateType;
    }

    public MoveStateType GetInverseStateType()
    {
        switch (CurrentState.GetStateType)
        {
            case MoveStateType.Up:
                return MoveStateType.Up;
            case MoveStateType.Down:
                return MoveStateType.Down;
        }
        return 0;
    }

    public Direction GetNextDirection(out bool success)
    {
        Direction newDirection;
        if (CurrentState.EvaluateDirection(out newDirection))
        {
            success = true;
            return newDirection;
        }
        else // if not success. so I try to switch movement algorithm up->down or down-> up
        {
            CurrentState.OnExit();

            CentipedeMovementState newState = SwitchState();
            CurrentState = newState;

            CurrentState.OnEnter();
            if (CurrentState.EvaluateDirection(out newDirection))
            {
                success = true;
                return newDirection;
            }
            else
            {
                // if can not find the best direction by move up state and move down state.
                // break the rules and find the avaliable direction.  
                Direction direction;
                if (BruteForceFindValidDirection(out direction))
                {
                    success = true;
                    return direction;
                }
                else
                {
                    success = false;
                    if (BruteForceAcceptableInvalidDirection(out direction))
                    {
                        return direction;
                    }
                    return Owner.Head.CurrentDirection;
                }

            }
        }

    }

    private bool BruteForceFindValidDirection(out Direction result)
    {
        result = Owner.Head.CurrentDirection;
        foreach (Direction direction in (Direction[])Enum.GetValues(typeof(Direction)))
        {
            if (Owner.IsCanMove(direction) == true)
            {
                result = direction;
                return true;
            }
        }
        return false;
    }

    private bool BruteForceAcceptableInvalidDirection(out Direction result)
    {
        MapManager _map = GameManager.Instance.MapManager;
        CentipedeCell head = Owner.Head;

        foreach (Direction direction in (Direction[])Enum.GetValues(typeof(Direction)))
        {
            Vector3 newPosition = head.CurrentPosition + direction.ToVector3() * _map.CellSize;
            GameObject objInDirection = _map.GetObjectFromLandData(newPosition.ToVector3Int());
            if (objInDirection == null)
            {
                continue;
            }

            ITakeDamageable takeDamageable = objInDirection.GetComponent<ITakeDamageable>();
            if (takeDamageable != null && takeDamageable.Team != Team.Enemy)
            {
                result = direction;
                return true;
            }
        }
        result = head.CurrentDirection;
        return false;
    }

    /// <summary>
    /// Switch state between MoveDown and MoveUp
    /// </summary>
    /// <returns></returns>
    private CentipedeMovementState SwitchState()
    {
        CentipedeMovementState newState;
        if (CurrentState.GetStateType == MoveStateType.Down)
        {
            newState = GetMovementState(MoveStateType.Up);
        }
        else
        {
            newState = GetMovementState(MoveStateType.Down);
        }

        return newState;
    }

    private CentipedeMovementState GetMovementState(MoveStateType state)
    {
        switch (state)
        {
            case MoveStateType.Up:
                return new MoveUpState(this);
            case MoveStateType.Down:
                return new MoveDownState(this);
            default:
                Debug.LogError("NotImplementedException");
                return null;
        }
    }

    public void SetOrderNextDirection(Direction direction)
    {
        CurrentState.SetOrderNextDirection(direction);
    }

}

