﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Centipede : MonoBehaviour
{
    public static event Action<Centipede> OnDeadEvent;
    public static event Action<CentipedeCell> OnCellDeadEvent;

    /// <summary>
    /// Cell per Second
    /// </summary>
    public int Speed => Head != null ? Head.status.Speed : 1;

    public List<CentipedeCell> CellList = new List<CentipedeCell>();

    public CentipedeAI Brain;

    public CentipedeCell Head => CellList.Count > 0 ? CellList[0] : null;
    public CentipedeCell Tail => CellList.Count > 0 ? CellList[CellList.Count - 1] : null;

    public bool IsDead => CellList.Count == 0;

    private Coroutine _moveCoroutine;
    private MapManager _map;

    public void Initialize(CentipedeAI brain)
    {
        _map = GameManager.Instance.MapManager;

        this.Brain = brain;
    }

    /// <summary>
    /// Add Cell To Tail if it is the first cell, It will be Head automatically.
    /// </summary>
    /// <param name="cell"></param>
    public void AddCellToTail(CentipedeCell cell)
    {
        if (Tail != null)
        {
            Tail.SetChild(cell);
            cell.SetParent(Tail);
            cell.SetSprite(CentipedeCell.CellType.Body);
        }
        else
        {
            cell.SetSprite(CentipedeCell.CellType.Head);
        }
        CellList.Add(cell);
    }
    #region Movement

    public void StartMoving()
    {
        _moveCoroutine = StartCoroutine(UpdateTick());
    }

    public void StopMoveing()
    {
        if (_moveCoroutine != null)
        {
            StopCoroutine(_moveCoroutine);
            _moveCoroutine = null;
        }
    }

    private IEnumerator UpdateTick()
    {
        float speed = Speed;
        while (!IsDead)
        {
            bool isSuccess;
            Direction newDirection = Brain.GetNextDirection(out isSuccess);
            Vector3 newPosition = Head.CurrentPosition + newDirection.ToVector3() * _map.CellSize;
            if (isSuccess)
            {
                CheckAndAttackToObjectAtGrid(newPosition);
                OnMoveSuccess(newDirection, newPosition);
            }
            else
            {
                OnMoveFail(newDirection, newPosition);
            }
            if (speed <= 0)
                yield break;
            else
                yield return new WaitForSeconds(1f / speed);
        }
    }

    private void CheckAndAttackToObjectAtGrid(Vector3 newPosition)
    {
        GameObject obj = _map.GetObjectFromLandData(newPosition.ToVector3Int());
        if (obj != null)
        {
            ITakeDamageable takeDamageable = obj.GetComponent<ITakeDamageable>();
            if (takeDamageable != null && takeDamageable.Team != Head.Team)
            {
                //Debug.Log(takeDamageable.gameObject.name);
                GameObject attacker = Head.gameObject;
                takeDamageable.TakeDamage(new DamageData(int.MaxValue, attacker));
            }
        }
    }

    private void OnMoveSuccess(Direction newDirection, Vector3 newPosition)
    {
        if (Head == null)
            return;
        Head.SetNewDirection(newDirection);
        Head.SetNewPosition(newPosition);
        Head.Move();
    }

    private void OnMoveFail(Direction newDirection, Vector3 newPosition)
    {
        GameObject obj = _map.GetObjectFromLandData(newPosition.ToVector3Int());

        if (obj != null)
        {
            ITakeDamageable takeDamageable = obj.GetComponent<ITakeDamageable>();
            if (takeDamageable != null && takeDamageable.Team != Head.Team)
            {
                takeDamageable.TakeDamage(new DamageData(int.MaxValue, Head.gameObject));
            }
            else
            {
                InverseMovement();
            }
        }
        else
        {
            InverseMovement();
        }
    }

    private void InverseMovement()
    {
        List<CentipedeCell> cacheCellList = new List<CentipedeCell>(CellList);
        CellList.Clear();
        for (int i = 0; i < cacheCellList.Count; i++)
        {
            Direction inverseDirection = cacheCellList[i].CurrentDirection.Inverse();
            cacheCellList[i].SetNewDirection(inverseDirection);
            cacheCellList[i].ClearChildAndParent();
        }
        cacheCellList.Reverse();
        for (int i = 0; i < cacheCellList.Count; i++)
        {
            AddCellToTail(cacheCellList[i]);
        }
        CentipedeAI.MoveStateType stateType = Brain.GetInverseStateType();
        Brain = new CentipedeAI(this, stateType);
    }

    #endregion

    public bool IsCanMove(Direction direction)
    {
        Vector3 newPosition = Head.CurrentPosition + direction.ToVector3() * _map.CellSize;
        GameObject objAtPosition;
        return IsCanMove(newPosition, out objAtPosition);
    }

    public bool IsCanMove(Direction direction, out GameObject objAtPosition)
    {
        Vector3 newPosition = Head.CurrentPosition + direction.ToVector3() * _map.CellSize;
        return IsCanMove(newPosition, out objAtPosition);
    }

    public bool IsCanMove(Vector3 newPosition, out GameObject objAtPosition)
    {
        objAtPosition = _map.GetObjectFromLandData(newPosition.ToVector3Int());
        if (objAtPosition != null)
        {
            ITakeDamageable takeDamageable = objAtPosition.GetComponent<ITakeDamageable>();
            if (takeDamageable != null && takeDamageable.Team == Team.Player)
            {
                return true;
            }
            return false;
        }
        return _map.IsCanWalkToPosition(newPosition);
    }

    public void OnCellDead(CentipedeCell cell)
    {
        OnCellDeadEvent?.Invoke(cell);

        bool isHead = Head == cell;

        _map.RemoveObjectFromLandData(cell.gameObject);
        cell.ClearChildAndParent();

        Centipede centipede = SplitCellToNewCentipedeAndRemoveCellFromList(cell);
        if (centipede != null)
        {
            // Clone the brain to new cell.
            CentipedeAI.MoveStateType stateType = Brain.GetMoveStateType();
            CentipedeAI brain = new CentipedeAI(centipede, stateType);

            Direction nextDirection;
            if (stateType == CentipedeAI.MoveStateType.Down)
            {
                nextDirection = Direction.Down;
            }
            else
            {
                nextDirection = Direction.Up;
            }
            brain.SetOrderNextDirection(nextDirection);

            centipede.Initialize(brain);
            centipede.StartMoving();
        }

        Destroy(cell.gameObject);
        if (IsDead)
        {
            StopMoveing();
            OnDeadEvent?.Invoke(this);
            Destroy(this.gameObject);
        }
    }

    private Centipede SplitCellToNewCentipedeAndRemoveCellFromList(CentipedeCell centipedeCell)
    {
        int index = CellList.FindIndex((cell) => cell == centipedeCell);
        int startIndex = index + 1;
        List<CentipedeCell> newCellList = CellList.CutLast(startIndex);
        CellList.RemoveAt(index);
        if (newCellList.Count > 0)
        {
            Centipede centipede = GameManager.Instance.MonsterSpawner.GenerateCentipedeByCellList(newCellList);
            return centipede;
        }
        else
        {
            return null;
        }
    }
}
