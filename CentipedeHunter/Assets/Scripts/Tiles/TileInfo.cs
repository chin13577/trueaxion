﻿
using System;
using UnityEngine;
using UnityEngine.Tilemaps;

public enum TileType { Land, Wall }
public class TileInfo
{
    public Tilemap TilemapMember { get; private set; }
    public Vector3 WorldLocation { get; private set; }
    public TileType TileType { get; private set; }

    public TileBase SelfTileBase { get; set; }
    public Vector3Int LocalPlace { get; set; }
    public GameObject Object { get; set; }

    public bool Walkable { get; set; }
    public bool IsUsedArea { get { return Object != null; } }

    public TileInfo(TileType type, Vector3Int localPlace, Tilemap tilemap)
    {
        TileType = type;
        TilemapMember = tilemap;
        LocalPlace = localPlace;

        WorldLocation = tilemap.CellToWorld(localPlace);
        ResetDefaultData();
    }

    public void ResetDefaultData()
    {
        Object = null;
        Walkable = true;
    }
}
