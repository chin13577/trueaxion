﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour, ITakeDamageable
{
    public Team Team => _team;
    private Team _team;

    private Direction _direction;
    private int _speed;

    public Vector3 CurrentPosition => transform.position;

    private Coroutine _moveCoroutine;
    private MapManager _map;


    public void Initialize(int speed, Team team, Direction direction)
    {
        _map = GameManager.Instance.MapManager;
        _speed = Mathf.Max(0, speed);
        this._team = team;
        this._direction = direction;
    }

    public void StartMoving()
    {
        StopMoving();
        _moveCoroutine = StartCoroutine(UpdateTick());
    }

    public void StopMoving()
    {
        if (_moveCoroutine != null)
        {
            StopCoroutine(_moveCoroutine);
            _moveCoroutine = null;
        }
    }

    private IEnumerator UpdateTick()
    {
        while (IsInBound())
        {
            if (_speed <= 0)
                break;
            else
                yield return new WaitForSeconds(1f / _speed);

            Vector3 newPosition = CurrentPosition + _direction.ToVector3() * _map.CellSize;
            if (AttackToObjectAtGrid(newPosition))
            {
                break;
            }
            else
            {
                if (IsCanMove(newPosition))
                {
                    SetNewPosition(newPosition);
                }
                else
                {
                    break;
                }
            }
        }
        DestroySelf();
    }

    private bool IsInBound()
    {
        return _map.DefaultArea.GetBounds().Contains(CurrentPosition);
    }

    private bool IsCanMove(Vector3 newPosition)
    {
        if (_map.IsCanWalkToPosition(newPosition) == false)
            return false;
        else if (_map.GetObjectFromLandData(newPosition.ToVector3Int()) != null)
            return false;

        return true;
    }

    public void SetAndAttackToPosition(Vector3 newPosition)
    {
        if (AttackToObjectAtGrid(newPosition) || IsCanMove(newPosition) == false)
        {
            DestroySelf();
        }
        else
        {
            SetNewPosition(newPosition);
            StartMoving();
        }
    }

    public bool AttackToObjectAtGrid(Vector3 newPosition)
    {
        GameObject obj = _map.GetObjectFromLandData(newPosition.ToVector3Int());
        if (obj != null)
        {
            ITakeDamageable takeDamageable = obj.GetComponent<ITakeDamageable>();
            if (takeDamageable != null && takeDamageable.Team != this.Team)
            {
                takeDamageable.TakeDamage(new DamageData(1, this.gameObject));
                return true;
            }
        }
        return false;
    }

    public void SetNewPosition(Vector3 newPosition)
    {
        _map.RemoveObjectFromLandData(this.gameObject);
        transform.position = newPosition;
        _map.SetObjectToLandData(this.gameObject);
    }

    public void TakeDamage(DamageData damage)
    {
        if (damage.Attacker != null)
        {
            ITakeDamageable takeDamageable = damage.Attacker.GetComponent<ITakeDamageable>();
            if (takeDamageable != null)
            {
                takeDamageable.TakeDamage(new DamageData(1, this.gameObject));
                DestroySelf();
            }
        }
    }

    public void DestroySelf()
    {
        _map.RemoveObjectFromLandData(this.gameObject);
        gameObject.SetActive(false);
    }

}
