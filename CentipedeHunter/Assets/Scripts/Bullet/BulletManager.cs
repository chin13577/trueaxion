﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager : MonoBehaviour
{
    [SerializeField] private GameObject _prefab;
    private FlexiblePooling _bulletPool;

    public void Initialize()
    {
        _bulletPool = new FlexiblePooling(this.transform, _prefab, 25);
    }

    public Bullet GetBullet()
    {
        Bullet result = _bulletPool.GetObject().GetComponent<Bullet>();
        result.gameObject.SetActive(true);
        return result;
    }

    public void ClearAllBullet()
    {
        List<GameObject> objects = _bulletPool.GetAllActiveObjects(obj => obj.activeSelf == true);
        for (int i = 0; i < objects.Count; i++)
        {
            objects[i].GetComponent<Bullet>().DestroySelf();
        }
    }
}
