﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    [ReadOnly]
    [SerializeField] private int _currentScore;

    [SerializeField] private GameUIManager _uiManager;

    public void Initialize()
    {
        _uiManager = GameManager.Instance.UIManager;
        _currentScore = 0;
        _uiManager.UpdateScore(_currentScore);
    }

    private void OnEnable()
    {
        Centipede.OnCellDeadEvent += Centipede_OnCellDeadEvent;
        Mushroom.OnDeadEvent += Mushroom_OnDeadEvent;
    }

    private void OnDisable()
    {
        Centipede.OnCellDeadEvent -= Centipede_OnCellDeadEvent;
        Mushroom.OnDeadEvent -= Mushroom_OnDeadEvent;
    }

    private void Centipede_OnCellDeadEvent(CentipedeCell cell)
    {
        AddScores(100);
    }

    private void Mushroom_OnDeadEvent(DamageData damageData)
    {
        if (damageData.Attacker == null)
            return;
        ITakeDamageable takeDamageable = damageData.Attacker.GetComponent<ITakeDamageable>();
        if (takeDamageable == null)
            return;
        if (takeDamageable.Team != Team.Player)
            return;

        AddScores(1);
    }

    private void AddScores(int score)
    {
        _currentScore += score;
        _uiManager.UpdateScore(_currentScore);
    }

}
