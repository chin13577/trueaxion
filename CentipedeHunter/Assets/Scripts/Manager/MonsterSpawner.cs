﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour
{
    public static event Action OnCentipedesListUpdate;

    [SerializeField] Sprite _tailSprite;
    [SerializeField] Sprite _headSprite;

    public CentipedeCell prefab;
    public List<Centipede> CentipedeList = new List<Centipede>();


    private void OnEnable()
    { 
        Centipede.OnDeadEvent += Centipede_OnDeadEvent;
    }

    private void OnDisable()
    { 
        Centipede.OnDeadEvent -= Centipede_OnDeadEvent;
    }

    public Centipede GenerateCentipedeOnTop(int amount)
    {
        List<CentipedeCell> cellList = new List<CentipedeCell>();

        MapManager map = GameManager.Instance.MapManager;

        int minX = -1 * map.DefaultArea.Left;
        int maxX = map.DefaultArea.Right;
        int minY = -1 * map.DefaultArea.Bottom;
        int maxY = map.DefaultArea.Top;

        for (int i = maxY - 1; i >= minY; i--)
        {
            for (int j = minX; j < maxX; j++)
            {
                if (amount > 0)
                {
                    Vector3 position = new Vector3(j, i, 0) + MapManager.CellOffset;
                    CentipedeCell cell = GameObject.Instantiate(prefab.gameObject).GetComponent<CentipedeCell>();
                    cell.SetNewPosition(position);
                    cellList.Add(cell);
                    amount--;
                }
                else
                {
                    break;
                }
            }
            if (amount <= 0)
                break;
        }
        cellList.Reverse();

        Centipede centipede = GenerateCentipedeByCellList(cellList);
        return centipede;
    }

    /// <summary>
    /// Generate New Centipede
    /// </summary>
    /// <param name="cellList">list at index 0 must be Head only.</param>
    /// <returns></returns>
    public Centipede GenerateCentipedeByCellList(List<CentipedeCell> cellList)
    {
        GameObject obj = new GameObject("Centipede_1", typeof(Centipede));
        Centipede centipede = obj.GetComponent<Centipede>();
        for (int i = 0; i < cellList.Count; i++)
        {
            centipede.AddCellToTail(cellList[i]);
            cellList[i].Initialize(centipede);
            cellList[i].transform.SetParent(centipede.transform);
        }
        centipede.Head.SetNewPosition(centipede.Head.CurrentPosition); 
        CentipedeList.Add(centipede);

        OnCentipedesListUpdate?.Invoke();
        return centipede;
    }
     
    private void Centipede_OnDeadEvent(Centipede centipede)
    { 
        CentipedeList.Remove(centipede);
        OnCentipedesListUpdate?.Invoke();
    }

    public void ClearAllCentipedes()
    {
        MapManager map = GameManager.Instance.MapManager;
        for (int i = 0; i < CentipedeList.Count; i++)
        {
            Centipede centipede = CentipedeList[i];
            centipede.StopMoveing(); 
            List<CentipedeCell> cellList = CentipedeList[i].CellList;
            for (int j = 0; j < cellList.Count; j++)
            {
                CentipedeCell cell = cellList[i];
                map.RemoveObjectFromLandData(cell.gameObject);
                cellList.RemoveAt(j);
                Destroy(cell.gameObject);
                j--;
            }
            CentipedeList.RemoveAt(i);
            Destroy(centipede.gameObject);
            i--;
        }
    }

    public Sprite GetHeadSprite()
    {
        return _headSprite;
    }

    public Sprite GetTailSprite()
    {
        return _tailSprite;
    }
}
