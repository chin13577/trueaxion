﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSetting : MonoBehaviour
{
    public const string CENTIPEDE_CELL_KEY = "CENTIPEDE_CELL_KEY";
    public const string CENTIPEDE_AMOUNT_KEY = "CENTIPEDE_AMOUNT_KEY";

    public const string PLAYER_KEY = "PLAYER_KEY";
    public const string BULLET_SPEED_KEY = "BULLET_SPEED_KEY";

    public const string MAP_AREA_KEY = "MAP_AREA_KEY";

    public const string MUSHROOM_KEY = "MUSHROOM_KEY";
    public const string MUSHROOM_SPAWN_CHANCE_KEY = "MUSHROOM_SPAWN_CHANCE_KEY";

    public static GameSetting Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameSetting>();
                _instance.Initialize();
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }

    private static GameSetting _instance;

    [Header("Centipede")]
    public int CentipedeCellAmount;
    [SerializeField] private Status _centipedeCellStatus;

    [Header("Mushroom")]
    [Range(0, 1f)] public float MushroomSpawnChance;
    [SerializeField] private Status _mushroomStatus;

    [Header("Player")]
    [SerializeField] private Status _playerStatus;
    public int BulletSpeed;


    [Header("Map")]
    [SerializeField] private Area _MapArea;

    private void Awake()
    {
        Time.timeScale = 1;
        if (_instance == null)
        {
            _instance = this;
            Initialize();
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
            return;
        }

    }

    private void Initialize()
    {
        try
        {
            LoadAll();
            SaveAll();
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
            ClearSetting();
            LoadAll();
        }
    }

    private void SaveStatus(string key, Status status)
    {
        PlayerPrefs.SetString(key, status.ToJSON());
    }

    public void LoadAll()
    {
        //centipede
        CentipedeCellAmount = PlayerPrefs.GetInt(CENTIPEDE_AMOUNT_KEY, CentipedeCellAmount);
        string centipedeCellJson = PlayerPrefs.GetString(CENTIPEDE_CELL_KEY, _centipedeCellStatus.ToJSON());
        _centipedeCellStatus = Status.CreateFromJSON(centipedeCellJson);

        //map
        string areaJson = PlayerPrefs.GetString(MAP_AREA_KEY, _MapArea.ToJSON());
        _MapArea = Area.CreateFromJSON(areaJson);

        //mushroom
        MushroomSpawnChance = PlayerPrefs.GetFloat(MUSHROOM_SPAWN_CHANCE_KEY, MushroomSpawnChance);
        string mushroomJson = PlayerPrefs.GetString(MUSHROOM_KEY, _mushroomStatus.ToJSON());
        _mushroomStatus = Status.CreateFromJSON(mushroomJson);

        //player
        string playerJson = PlayerPrefs.GetString(PLAYER_KEY, _playerStatus.ToJSON());
        _playerStatus = Status.CreateFromJSON(playerJson);
        BulletSpeed = PlayerPrefs.GetInt(BULLET_SPEED_KEY, BulletSpeed);
    }

    [ContextMenu("SaveAll")]
    public void SaveAll()
    {
        //centipede
        PlayerPrefs.SetInt(CENTIPEDE_AMOUNT_KEY, CentipedeCellAmount);
        SaveStatus(CENTIPEDE_CELL_KEY, _centipedeCellStatus);

        //map
        PlayerPrefs.SetString(MAP_AREA_KEY, _MapArea.ToJSON());

        //mushroom
        PlayerPrefs.SetFloat(MUSHROOM_SPAWN_CHANCE_KEY, MushroomSpawnChance);
        SaveStatus(MUSHROOM_KEY, _mushroomStatus);

        //player
        SaveStatus(PLAYER_KEY, _playerStatus);
        PlayerPrefs.SetInt(BULLET_SPEED_KEY, BulletSpeed);
    }

    [ContextMenu("ClearAll")]
    public void ClearSetting()
    {
        PlayerPrefs.DeleteAll();
    }

    public void SetCentipedeCellStatus(Status status)
    {
        _centipedeCellStatus = status;
    }

    public Status GetCentipedeCellStatus()
    {
        return new Status(_centipedeCellStatus);
    }

    public void SetMushroomStatus(Status status)
    {
        _mushroomStatus = status;
    }

    public Status GetMushroomStatus()
    {
        return new Status(_mushroomStatus);
    }

    public void SetPlayerStatus(Status status)
    {
        _playerStatus = status;
    }

    public Status GetPlayerStatus()
    {
        return new Status(_playerStatus);
    }

    public Area GetMapArea()
    {
        return _MapArea;
    }

    public Area SetMapArea(Area area)
    {
        return this._MapArea = area;
    }
}
