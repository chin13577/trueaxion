﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StarterUIController : MonoBehaviour
{
    public ConfigUIController ConfigUI;
    public GameObject MainPanel; 
    private GameSetting _gameSetting;
     
    private void Start()
    {
        _gameSetting = GameSetting.Instance;
        MainPanel.SetActive(true);

        ConfigUI.SetData(_gameSetting);
        ConfigUI.SetEvent(OnClickSaveAll, OnClickClearAll);
    }
     
    public void OnClickPlay()
    {
        MainPanel.SetActive(false);
        GameManager.Instance.InitializeGame();
    }

    public void OnClickConfigPanel()
    {
        ConfigUI.ShowUI();
    }

    public void OnResetGame()
    {
        SceneManager.LoadScene(0);
    }

    private void OnClickClearAll()
    {
        _gameSetting.LoadAll();
        ConfigUI.SetData(_gameSetting);
    }

    private void OnClickSaveAll()
    {
        _gameSetting.SetCentipedeCellStatus(ConfigUI.CentipedeCellStatusConfig.GetStatus());
        _gameSetting.SetPlayerStatus(ConfigUI.PlayerStatusConfig.GetStatus());
        _gameSetting.SetMushroomStatus(ConfigUI.MushroomStatusConfig.GetStatus());

        _gameSetting.SetMapArea(ConfigUI.AreaConfig.GetArea());
        _gameSetting.MushroomSpawnChance = ConfigUI.MushroomSpawnChance.value;

        _gameSetting.BulletSpeed = ConfigUI.BulletSpeedField.GetValue();
        _gameSetting.CentipedeCellAmount = ConfigUI.CentipedeAmountField.GetValue();

        //_gameSetting.SaveAll();
    }
}
