﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<GameManager>();
            return _instance;
        }
    }

    public static GameManager _instance;
    public GameUIManager UIManager;


    public MapManager MapManager;
    public CameraBound2D CameraBound;
    public MonsterSpawner MonsterSpawner;
    public MushroomGenerator MushroomSpawner;
    public BulletManager BulletManager;
    public ScoreManager ScoreManager;

    [SerializeField] private Player PlayerPrefab;
    [HideInInspector] public Player Player;
    public PlayerController Controller;

    [ReadOnly]
    public bool IsPause;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
    }

    private void OnEnable()
    {
        MonsterSpawner.OnCentipedesListUpdate += MonsterSpawner_OnCentipedesListUpdate;
        Player.OnPlayerDead += Player_OnPlayerDead;
    }

    private void OnDisable()
    {
        MonsterSpawner.OnCentipedesListUpdate -= MonsterSpawner_OnCentipedesListUpdate;
        Player.OnPlayerDead -= Player_OnPlayerDead;
    }

    public void TogglePauseGame()
    {
        IsPause = !IsPause;
        if (IsPause)
        {
            UIManager.PausePanel.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
            UIManager.PausePanel.SetActive(false);
        }
    }

    public void InitializeGame()
    {
        MapManager.Initialize();
        BulletManager.Initialize();
        SettingCamera();
        ScoreManager.Initialize();
        SettingPlayer();
        SettingEnemy();

        UIManager.SetActiveStatusPanel(true);

        MushroomSpawner.Initialize();
        MushroomSpawner.GenerateMushroom();
    }

    private void SettingEnemy()
    {
        Centipede centipede = MonsterSpawner.GenerateCentipedeOnTop(GameSetting.Instance.CentipedeCellAmount);
        // Can customize CentipedeAI with your own.
        CentipedeAI brain = new CentipedeAI(centipede);
        centipede.Initialize(brain);

        centipede.StartMoving();
    }

    private void SettingPlayer(bool spawnPlayer = true)
    {
        if (spawnPlayer)
        {
            Player = Instantiate(PlayerPrefab.gameObject).GetComponent<Player>();
            Player.Initialize(GameSetting.Instance.GetPlayerStatus());
        }
        Player.SetPosition(MapManager.GetPlayerSpawnPos());
        Controller.SetPlayer(Player);
    }

    private void SettingCamera()
    {
        Bounds bound = GameSetting.Instance.GetMapArea().GetBounds();

        // make player see a few pixels of walls.
        bound.Expand(0.5f);

        CameraBound.UpdateCameraBound(bound);
    }

    private void MonsterSpawner_OnCentipedesListUpdate()
    {
        if (MonsterSpawner.CentipedeList.Count <= 0)
            GameOver(WinCondition.Win);
    }

    private void Player_OnPlayerDead(Player player)
    {
        if (player.Status.IsDead)
        {
            GameOver(WinCondition.Lose);
            return;
        }

        ClearCentipede();
        SettingPlayer(false);
        SettingEnemy();
    }

    private void GameOver(WinCondition winCondition)
    {
        Debug.Log("You " + winCondition);
        Controller.SetPlayer(null);
        BulletManager.ClearAllBullet();
        UIManager.ShowGameOverPopup();
        Controller.SetListenerPressKeyDownEvent(KeyCode.Space, () =>
        {
            UIManager.HideGameOverPopup();
            SceneManager.LoadScene(0);
        });
    }

    private void ClearCentipede()
    {
        MonsterSpawner.ClearAllCentipedes();
    }

}
