﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapManager : MonoBehaviour
{
    public float CellSize { get { return LandTileMap.cellSize.x; } }
    public static readonly Vector3 CellOffset = new Vector2(0.5f, 0.5f);

    [SerializeField] private Tile _DirtTile;
    [SerializeField] private Tile _WallTile;

    [ReadOnly] public Area DefaultArea;

    public Tilemap LandTileMap;
    public Tilemap WallTileMap;

    public Dictionary<Vector3, TileInfo> LandData = new Dictionary<Vector3, TileInfo>();

    [Header("Config")]
    public bool SetLandSprite;

    public void Initialize()
    {
        DefaultArea = GameSetting.Instance.GetMapArea();
        CreateLand();
        ReadLandData();
        CreateWalls();
    }

    #region Generate Map

    private void CreateLand()
    {
        int minX = -1 * DefaultArea.Left;
        int maxX = DefaultArea.Right;
        int minY = -1 * DefaultArea.Bottom;
        int maxY = DefaultArea.Top;

        for (int i = minY; i < maxY; i++)
        {
            for (int j = minX; j < maxX; j++)
            {
                Vector3Int position = new Vector3Int(j, i, 0);
                if (SetLandSprite)
                {
                    LandTileMap.SetTile(position, _DirtTile);
                }
            }
        }
    }

    private void CreateWalls()
    {
        int minX = (-1 * DefaultArea.Left) - 1;
        int maxX = (DefaultArea.Right) + 1;
        int minY = (-1 * DefaultArea.Bottom) - 1;
        int maxY = (DefaultArea.Top) + 1;

        for (int i = minY; i < maxY; i++)
        {
            for (int j = minX; j < maxX; j++)
            {
                Vector3Int position = new Vector3Int(j, i, 0);
                if (i == minY || i == maxY - 1)
                {
                    // Create All 
                    WallTileMap.SetTile(position, _WallTile);
                    SetWalkableToLandData(position, false);
                }
                if (j == minX || j == maxX - 1)
                {
                    // Create only left and right. 
                    WallTileMap.SetTile(new Vector3Int(j, i, 0), _WallTile);
                    SetWalkableToLandData(position, false);
                }
            }
        }
    }

    public Vector3 GetPlayerSpawnPos()
    {
        Vector3 centerPos = DefaultArea.GetBounds().center;
        Vector3 pos = (centerPos - new Vector3(0, DefaultArea.GetSize().y / 2f));
        Debug.LogFormat("center : {0}, target : {1}", centerPos, pos);
        return pos;
    }

    public List<TileInfo> GetPlayerZone(Predicate<TileInfo> predicate = null)
    {
        if (predicate == null)
        {
            predicate = (data) => true;
        }
        float maxPosY = GameManager.Instance.Player.MaxPlayerPosY;
        return GetTileInfoList(LandData, (tile) => tile.WorldLocation.y <= maxPosY && predicate(tile));
    }

    #endregion

    #region Map Data

    private void ReadLandData()
    {
        LandData = ReadTilesData(LandTileMap, TileType.Land);
    }

    private Dictionary<Vector3, TileInfo> ReadTilesData(Tilemap tilemap, TileType type)
    {
        Dictionary<Vector3, TileInfo> result = new Dictionary<Vector3, TileInfo>();
        int minX = -1 * DefaultArea.Left;
        int maxX = DefaultArea.Right;
        int minY = -1 * DefaultArea.Bottom;
        int maxY = DefaultArea.Top;

        for (int i = minY; i < maxY; i++)
        {
            for (int j = minX; j < maxX; j++)
            {
                Vector3Int localPlace = new Vector3Int(j, i, 0);
                
                TileInfo tile = new TileInfo(type, localPlace, tilemap);
                tile.TilemapMember.SetTileFlags(tile.LocalPlace, TileFlags.None);
                result.Add(tile.WorldLocation, tile);
            }
        }
        return result;
    }

    public TileInfo GetTileInfo(Dictionary<Vector3, TileInfo> dict, Vector3Int pos)
    {
        TileInfo tile;

        if (dict.TryGetValue(pos, out tile))
            return tile;
        return null;
    }

    public List<TileInfo> GetTileInfoList(Dictionary<Vector3, TileInfo> tileDict, Predicate<TileInfo> predicate = null)
    {
        List<TileInfo> result = new List<TileInfo>();
        if (predicate == null)
        {
            predicate = (data) => true;
        }
        foreach (var data in tileDict)
        {
            TileInfo tile = data.Value;
            if (predicate(tile))
            {
                result.Add(tile);
            }
        }
        return result;
    }

    public bool IsCanWalkToPosition(Vector3 position)
    {
        TileInfo tile;
        Vector3Int targetPos = position.ToVector3Int();
        if (LandData.TryGetValue(targetPos, out tile))
        {
            return tile.Walkable;
        }
        return false;
    }

    public void SetWalkableToLandData(Vector3Int pos, bool walkable)
    {
        TileInfo tile;
        if (LandData.TryGetValue(pos, out tile))
        {
            tile.Walkable = walkable;
        }
    }

    public GameObject GetObjectFromLandData(Vector3Int pos)
    {
        TileInfo tile;
        if (LandData.TryGetValue(pos, out tile))
            return tile.Object;
        else
            return null;
    }

    public void SetObjectToLandData(GameObject obj)
    {
        TileInfo tile;
        Vector3Int targetPos = obj.transform.position.ToVector3Int();
        if (LandData.TryGetValue(targetPos, out tile))
        {
            tile.Object = obj.gameObject;
        }
    }

    public void RemoveObjectFromLandData(GameObject obj)
    {
        Vector3Int targetPos = obj.transform.position.ToVector3Int();
        TileInfo tile;
        if (LandData.TryGetValue(targetPos, out tile))
        { 
            if (tile.Object == obj)
            {
                tile.ResetDefaultData();
            }
        }
    }

    #endregion

    #region Debug

    [ContextMenu("PlayerZone")]
    public void DebugPlayerMoveable()
    {
        List<TileInfo> tiles = GetPlayerZone();
        foreach (var tile in tiles)
        {
            tile.TilemapMember.SetColor(tile.LocalPlace, Color.green);
        } 
    }

    [ContextMenu("UnWalkable")]
    public void DebugUnWalkable()
    {
        SetTilesColor(LandData, Color.red, (data) => data.Walkable == false);
    }

    [ContextMenu("HaveObject")]
    public void DebugHaveObject()
    {
        SetTilesColor(LandData, Color.gray, (data) => data.IsUsedArea == true);
    }

    [ContextMenu("Clear Color")]
    public void ClearColor()
    {
        SetTilesColor(LandData, new Color(1, 1, 1, 1));
    }

    public void SetTilesColor(Dictionary<Vector3, TileInfo> tileDict, Color color, Predicate<TileInfo> predicate = null)
    {
        if (predicate == null)
        {
            predicate = (data) => true;
        }
        foreach (var data in tileDict)
        {
            TileInfo tile = data.Value;
            if (predicate(tile))
            {
                data.Value.TilemapMember.SetColor(tile.LocalPlace, color);
            }
        }
    }

    #endregion
}
