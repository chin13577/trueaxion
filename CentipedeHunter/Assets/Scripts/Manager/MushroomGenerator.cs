﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MushroomGenerator : MonoBehaviour
{
    [SerializeField] private Mushroom prefab;
    public FlexiblePooling MushroomPooling;

    public List<Sprite> MushroomSprite = new List<Sprite>();

    public void Initialize()
    {
        MushroomPooling = new FlexiblePooling(this.transform, prefab.gameObject, 10);
    }

    public Sprite GetMushroomSprite(int currentHP)
    {
        int index = Mathf.Clamp(currentHP - 1, 0, MushroomSprite.Count - 1);
        return MushroomSprite[index];
    }

    public Mushroom GetMushroom()
    {
        Mushroom result = MushroomPooling.GetObject().GetComponent<Mushroom>();
        result.gameObject.SetActive(true);
        return result;
    }

    #region Generate Mushroom

    public void GenerateMushroom()
    {
        MapManager map = GameManager.Instance.MapManager;
        List<TileInfo> emptyList = FilterSpawnablePositions(map);

        DateTime time = DateTime.Now;
        float spawnChance = 1 - GameSetting.Instance.MushroomSpawnChance;
        for (int i = 0; i < emptyList.Count; i++)
        {
            Vector3 pos = emptyList[i].WorldLocation + MapManager.CellOffset;
            float randomValue = Mathf.PerlinNoise(pos.x + time.Second, pos.y - time.Second);
            if (randomValue > spawnChance)
            {
                Mushroom shroom = GetMushroom();
                shroom.Initialize(GameSetting.Instance.GetMushroomStatus());
                shroom.SetPosition(pos);
            }
        }
    }

    private List<TileInfo> FilterSpawnablePositions(MapManager map)
    {
        Bounds bounds = map.DefaultArea.GetBounds();
        int minX = Convert.ToInt32(bounds.min.x);
        int maxX = Convert.ToInt32(bounds.max.x) - 1;
        List<TileInfo> emptyList = FilterTilesInRegion(map, minX, maxX);
        return emptyList;
    }

    private List<TileInfo> FilterTilesInRegion(MapManager map, int minX, int maxX)
    {
        return map.GetTileInfoList(
            map.LandData,
            (info) =>
            {
                return info.IsUsedArea == false &&
                info.LocalPlace.x > minX &&
                info.LocalPlace.x < maxX &&
                info.WorldLocation.y > GameManager.Instance.Player.transform.position.y;
            }
        );
    }
    #endregion

}
