﻿
using UnityEngine;

public struct DamageData
{
    public int Damage;
    public GameObject Attacker; 

    public DamageData(int damage, GameObject attacker)
    {
        Damage = damage;
        Attacker = attacker;
    }
}