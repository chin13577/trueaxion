﻿using UnityEngine;
using System;

[Serializable]
public struct Area
{
    [Range(1, 100)] public int Left;
    [Range(1, 100)] public int Right;
    [Range(1, 100)] public int Top;
    [Range(1, 100)] public int Bottom;

    public Area(int left, int right, int top, int bottom)
    {
        Left = left;
        Right = right;
        Top = top;
        Bottom = bottom;
    }
     
    public Bounds GetBounds()
    {
        float centerX = Convert.ToSingle(Right - Left) / 2f;
        float centerY = Convert.ToSingle(Top - Bottom) / 2f;
        return new Bounds(new Vector3(centerX, centerY), GetSize());
    }

    public Vector3 GetSize()
    {
        return new Vector3(Right + Left, Top + Bottom, 0);
    }

    public static Area CreateFromJSON(string json)
    {
        return JsonUtility.FromJson<Area>(json);
    }

    public string ToJSON()
    {
        return JsonUtility.ToJson(this);
    }
}
