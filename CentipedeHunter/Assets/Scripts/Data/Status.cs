﻿using System;
using UnityEngine;

[Serializable]
public class Status
{
    [ReadOnly] public int HP;
    public int MaxHP;
    public int Speed;
    public bool IsDead => HP <= 0;

    public Status()
    {
        MaxHP = Mathf.Max(1, MaxHP);
        HP = MaxHP;
        Speed = Mathf.Max(0, Speed);
    }

    public Status(int maxHp, int speed)
    {
        MaxHP = Mathf.Max(1, maxHp);
        HP = maxHp;
        Speed = Mathf.Max(0, speed);
    }

    public Status(Status status)
    {
        MaxHP = Mathf.Max(1, status.MaxHP);
        HP = MaxHP;
        Speed = Mathf.Max(0, status.Speed);
    }

    public static Status CreateFromJSON(string json)
    {
        return JsonUtility.FromJson<Status>(json);
    }

    public string ToJSON()
    {
        HP = MaxHP;
        return JsonUtility.ToJson(this);
    }
}
