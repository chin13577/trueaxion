﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUIManager : MonoBehaviour
{
    [SerializeField] private GameObject _gameOverPanel;

    [SerializeField] private GameObject _topbarPanel;
    [SerializeField] private ScoreUI _scoreUI;
    [SerializeField] private HPUI _hpUI;

    public ConfigUIController ConfigUI;
    public GameObject PausePanel;

    public void UpdateScore(int currentScore)
    {
        _scoreUI.UpdateScore(currentScore);
    }

    public void UpdatePlayerHP(int currentHP)
    {
        _hpUI.UpdateHP(currentHP);
    }

    public void SetActiveStatusPanel(bool isActive)
    {
        _topbarPanel.SetActive(isActive);
    }

    public void ShowGameOverPopup()
    {
        _gameOverPanel.SetActive(true);
    }

    public void HideGameOverPopup()
    {
        _gameOverPanel.SetActive(false);
    }
}
