﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(InputField))]
public class ClampInputField : MonoBehaviour
{
    public int MinValue;
    public InputField Field;

    private void OnEnable()
    {
        Field.onValueChanged.RemoveAllListeners();
        Field.onValueChanged.AddListener((input) => OnValueChange(Field, input, MinValue));
    }

    private void OnDisable()
    {
        Field.onValueChanged.RemoveAllListeners();
    }

    public int GetValue()
    {
        return Field.text.ToInt32();
    }

    public void SetValue(int value)
    {
        value = Mathf.Max(value, MinValue);
        Field.text = value.ToString();
    }

    private void OnValueChange(InputField field, string inputValue, int minValue)
    {
        int amount = 0;
        ClampIntInputValue(inputValue, out amount);
        field.text = amount.ToString();
    }

    private bool ClampIntInputValue(string inputValue, out int result)
    {
        if (inputValue == "")
        {
            result = 0;
            return false;
        }
        result = Convert.ToInt32(inputValue);
        result = Mathf.Max(0, result);
        return true;
    }

}
