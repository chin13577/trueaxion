﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPUI : MonoBehaviour
{
    [SerializeField] private Text hpText;
    public void UpdateHP(int currentHP)
    {
        hpText.text = currentHP.ToString();
    }

    public void ShowUI()
    {
        this.gameObject.SetActive(true);
    }

    public void HideUI()
    {
        this.gameObject.SetActive(false);
    }
}
