﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AreaConfigUI : MonoBehaviour
{
    public InputField Left;
    public InputField Right;
    public InputField Top;
    public InputField Bottom;

    [SerializeField] [ReadOnly] private Area area;
    private void Awake()
    {
        Left.onValueChanged.RemoveAllListeners();
        Left.onValueChanged.AddListener((input) => OnValueChange(Left, input, 1));

        Right.onValueChanged.RemoveAllListeners();
        Right.onValueChanged.AddListener((input) => OnValueChange(Right, input, 1));

        Top.onValueChanged.RemoveAllListeners();
        Top.onValueChanged.AddListener((input) => OnValueChange(Top, input, 1));

        Bottom.onValueChanged.RemoveAllListeners();
        Bottom.onValueChanged.AddListener((input) => OnValueChange(Bottom, input, 1));
    }

    private void OnValueChange(InputField field, string inputValue, int minValue)
    {
        int amount = 0;
        ConvertInputValueToInt(inputValue, out amount);
        amount = Mathf.Max(minValue, amount);
        field.text = amount.ToString();
    }

    private bool ConvertInputValueToInt(string inputValue, out int result)
    {
        if (inputValue == "")
        {
            result = 0;
            return false;
        }
        result = Convert.ToInt32(inputValue);
        result = Mathf.Max(0, result);
        return true;
    }

    public void SetArea(Area area)
    {
        this.area = area;

        Left.text = area.Left.ToString();
        Right.text = area.Right.ToString();
        Top.text = area.Top.ToString();
        Bottom.text = area.Bottom.ToString();
    }

    public Area GetArea()
    {
        int left = Left.text.ToInt32();
        int right = Right.text.ToInt32();
        int top = Top.text.ToInt32();
        int bottom = Bottom.text.ToInt32();

        return new Area(left, right, top, bottom);
    }
}
