﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusConfigUI : MonoBehaviour
{
    public InputField HPInputField;
    public InputField SpeedInputField;

    [SerializeField] [ReadOnly] private Status status;
    private void Awake()
    {
        status = new Status();
        HPInputField.onValueChanged.RemoveAllListeners();
        HPInputField.onValueChanged.AddListener((input) => OnValueChange(HPInputField, input, 1));

        SpeedInputField.onValueChanged.RemoveAllListeners();
        SpeedInputField.onValueChanged.AddListener((input) => OnValueChange(SpeedInputField, input, 0));
    }

    private void OnValueChange(InputField field, string inputValue, int minValue)
    {
        int amount = 0;
        ConvertInputValueToInt(inputValue, out amount);
        amount = Mathf.Max(minValue, amount);
        field.text = amount.ToString();
    }

    private bool ConvertInputValueToInt(string inputValue, out int result)
    {
        result = 0;
        if (inputValue == "")
        {
            return false;
        }
        int.TryParse(inputValue, out result);
        result = Mathf.Max(0, result);
        return true;
    }

    public void SetStatus(Status status)
    {
        HPInputField.text = status.MaxHP.ToString();
        SpeedInputField.text = status.Speed.ToString();
    }

    public Status GetStatus()
    {
        return new Status(HPInputField.text.ToInt32(), SpeedInputField.text.ToInt32());
    }
}
