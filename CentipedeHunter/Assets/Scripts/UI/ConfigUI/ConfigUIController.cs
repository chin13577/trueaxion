﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ConfigUIController : MonoBehaviour
{
    public ClampInputField CentipedeAmountField;
    public ClampInputField BulletSpeedField;

    public Slider MushroomSpawnChance;
    public AreaConfigUI AreaConfig;

    public StatusConfigUI PlayerStatusConfig;
    public StatusConfigUI MushroomStatusConfig;
    public StatusConfigUI CentipedeCellStatusConfig;

    public Button SaveButton;
    public Button ClearButton;

    public void SetData(GameSetting gameSetting)
    {
        CentipedeAmountField.SetValue(gameSetting.CentipedeCellAmount);
        BulletSpeedField.SetValue(gameSetting.BulletSpeed);

        MushroomSpawnChance.value = gameSetting.MushroomSpawnChance; 
        AreaConfig.SetArea(gameSetting.GetMapArea());

        PlayerStatusConfig.SetStatus(gameSetting.GetPlayerStatus());
        MushroomStatusConfig.SetStatus(gameSetting.GetMushroomStatus());
        CentipedeCellStatusConfig.SetStatus(gameSetting.GetCentipedeCellStatus());
    }

    public void SetEvent(UnityAction onSaveAll, UnityAction onClearAll)
    {
        SaveButton.onClick.RemoveAllListeners();
        SaveButton.onClick.AddListener(onSaveAll);

        ClearButton.onClick.RemoveAllListeners();
        ClearButton.onClick.AddListener(onClearAll);
    }

    public void ShowUI()
    {
        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }
}
