﻿// Global enums.

public enum Direction { Up, Right, Down, Left }
public enum WinCondition { Win, Lose }
public enum Team { Player, Neutral, Enemy }