﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mushroom : MonoBehaviour, ITakeDamageable
{
    public static event Action<DamageData> OnDeadEvent;
    public Status Status;
    public Team Team => Team.Neutral;

    [SerializeField] private SpriteRenderer _spriteRenderer;

    public MapManager Map
    {
        get
        {
            if (_map == null)
                _map = GameManager.Instance.MapManager;
            return _map;
        }
    }
    private MapManager _map;

    public void Initialize(Status status)
    {
        Status = status;
    }

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
        Map.SetObjectToLandData(this.gameObject);
    }

    public void ResetData()
    {
        UpdateSprite(Status.MaxHP);
        Map.RemoveObjectFromLandData(this.gameObject);
    }

    public void TakeDamage(DamageData damageData)
    {
        Status.HP -= damageData.Damage;
        UpdateSprite(Status.HP);
        if (Status.IsDead)
        {
            ResetData();
            OnDeadEvent?.Invoke(damageData);
            gameObject.SetActive(false);
        }
    }

    private void UpdateSprite(int currentHp)
    {
        MushroomGenerator mushroomSpawner = GameManager.Instance.MushroomSpawner;
        _spriteRenderer.sprite = mushroomSpawner.GetMushroomSprite(currentHp);
    }
}
