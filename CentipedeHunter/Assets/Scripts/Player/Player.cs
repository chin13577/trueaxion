﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, ITakeDamageable
{
    public static event Action<Player> OnPlayerDead;

    public Team Team => Team.Player;
    public Status Status;

    public float MaxPlayerPosY { get; set; }

    public MapManager Map
    {
        get
        {
            if (_map == null)
                _map = GameManager.Instance.MapManager;
            return _map;
        }
    }
    public int MoveSpeed => Status.Speed;
    public Vector3 CurrentPosition => transform.position;

    private MapManager _map;
    private BulletManager _bulletManager;
    private GameUIManager _uiManager;

    public void Initialize(Status status)
    {
        Status = status;

        _uiManager = GameManager.Instance.UIManager;
        _uiManager.UpdatePlayerHP(Status.HP);

        _bulletManager = GameManager.Instance.BulletManager;

        float boundMinY = Map.DefaultArea.GetBounds().min.y;
        Vector2 screenHight = new Vector2(0, Screen.height * 0.15f);
        float high = Mathf.Abs(boundMinY - Camera.main.ScreenToWorldPoint(screenHight).y);
        MaxPlayerPosY = Mathf.FloorToInt(boundMinY + high);
        Debug.Log("high " + high);
    }

    public void SetPosition(Vector3 position)
    {
        position = position.ToVector3Int();
        if (IsCanMove(position))
        {
            Map.RemoveObjectFromLandData(this.gameObject);
            transform.position = position + MapManager.CellOffset;
            Map.SetObjectToLandData(this.gameObject);
        }
    }

    public bool IsCanMove(Vector3 newPosition)
    {
        if (Status.IsDead)
            return false;
        else if (newPosition.y > MaxPlayerPosY)
            return false;
        else if (Map.IsCanWalkToPosition(newPosition) == false)
            return false;
        else if (Map.GetObjectFromLandData(newPosition.ToVector3Int()) != null)
            return false;

        return true;
    }

    public void Shoot()
    {
        Bullet bullet = _bulletManager.GetBullet();
        bullet.Initialize(GameSetting.Instance.BulletSpeed, this.Team, Direction.Up);

        Vector3 bulletSpawnPos = CurrentPosition + Direction.Up.ToVector3();
        bullet.SetAndAttackToPosition(bulletSpawnPos);
    }

    public void TakeDamage(DamageData damageData)
    {
        if (damageData.Damage <= 0)
            return;

        Status.HP -= 1;
        _uiManager.UpdatePlayerHP(Status.HP);

        Map.RemoveObjectFromLandData(this.gameObject);
        OnPlayerDead?.Invoke(this);
    }
}
