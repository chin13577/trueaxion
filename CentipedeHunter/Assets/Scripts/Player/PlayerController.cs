﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    public delegate void OnPressKeyDownHandler();

    private Player _player;
    private Vector2 oldInputValue;

    private float _interval;
    private float _timer;

    private ListenerPressKeyDownEvent _listenerPressKeyDownEvent;

    public void SetPlayer(Player player)
    {
        this._player = player;
        if (player == null)
            return;

        _interval = 1f / player.MoveSpeed;
        _timer = _interval;
    }

    void Update()
    {
        if (GameManager.Instance.IsPause)
            return;
        if (_listenerPressKeyDownEvent != null)
        {
            if (_listenerPressKeyDownEvent.Excecute() == true)
            {
                ClearListenerPressKeyDownEvent();
            }
            else
            {
                return;
            }
        }

        if (_player == null)
        {
            oldInputValue = Vector2.zero;
            return;
        }

        Vector2 inputValue = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        if (oldInputValue != inputValue)
        {
            OnInputValueChange(inputValue);
            oldInputValue = inputValue;
        }
        UpdateInputInterval();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        _player.Shoot();
    }

    public void SetListenerPressKeyDownEvent(KeyCode targetKey, OnPressKeyDownHandler onPressKey)
    {
        _listenerPressKeyDownEvent = new ListenerPressKeyDownEvent(onPressKey, targetKey);
    }

    private void ClearListenerPressKeyDownEvent()
    {
        _listenerPressKeyDownEvent = null;
    }

    private void UpdateInputInterval()
    {
        _timer -= Time.deltaTime;
        if (_timer <= 0)
        {
            OnInputValueChange(oldInputValue);
        }
    }

    private void OnInputValueChange(Vector3 inputValue)
    {
        _timer = _interval;
        if (inputValue == Vector3.zero)
            return;
        _player.SetPosition(_player.CurrentPosition + inputValue);
    }
}

/// <summary>
/// The class that handle about checking PressKeyDown Event.
/// </summary>
public class ListenerPressKeyDownEvent
{
    private KeyCode _targetKey;
    private PlayerController.OnPressKeyDownHandler _onPressKeyDown;

    public ListenerPressKeyDownEvent(PlayerController.OnPressKeyDownHandler onPressKeyDown, KeyCode targetKey)
    {
        this._targetKey = targetKey;
        this._onPressKeyDown = onPressKeyDown;
    }

    public bool Excecute()
    {
        if (Input.GetKeyDown(_targetKey))
        {
            _onPressKeyDown?.Invoke();
            return true;
        }
        return false;
    }
}